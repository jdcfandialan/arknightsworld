<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Item;
use \App\Category;
use Session;

class ItemController extends Controller
{
   
	 //catalog index
    public function index(){

    	$items = Item::all();

    	return view('catalog', compact('items'));
    }

    //add an item form
    public function create(){

    	$categories = Category::all();

    	return view ('adminviews.additem', compact('categories'));
    }

    //add an item to the database
    public function store(Request $req){
    	
    	//form validation
    	$rules = array(
    		"name" => "required",
    		"description" => "required",
    		"price" => "required|numeric",
    		"category_id" => "required",
    		"image" => "required|image|mimes:jpeg, jpg, png, gif, tiff, tif, webp"
    	);
    	
    	$this->validate($req, $rules);
    	
    	//form capture
    	$newOperator = new Item;
    	
    	$newOperator->name = $req->name;
    	$newOperator->description = $req->description;
    	$newOperator->price = $req->price;
    	$newOperator->category_id = $req->category_id;

    	//image handling
    	$image = $req->file('image');
    	$image_name = time().".".$image->getClientOriginalExtension();

    	$destination = "images/";

    	$image->move($destination, $image_name);

    	$newOperator->imgPath = $destination.$image_name;

    	//save the new item to the database
    	$newOperator->save();

    	Session::flash("message", "$newOperator->name has been addded!");

    	//return to catalog
    	return redirect('/operators');
    }

    public function destroy($id){
    	$operator = Item::find($id);
    	$operator->delete();

    	Session::flash("message", "$operator->name has been deleted!");

    	return redirect('/operators');
    }

    public function edit($id){
    	$item = Item::find($id);
    	$categories = Category::all();

    	return view('/adminviews.edititem', compact('item', 'categories'));
    }

    public function update($id, Request $req){
    	//form validation
    	$rules = array(
    		"name" => "required",
    		"description" => "required",
    		"price" => "required|numeric",
    		"category_id" => "required",
    		"image" => "image|mimes:jpeg, jpg, png, gif, tiff, tif, webp"
    	);

    	$this->validate($req, $rules);

    	$editOperator = Item::find($id);

    	$editOperator->name = $req->name;
    	$editOperator->description = $req->description;
    	$editOperator->price = $req->price;
    	$editOperator->category_id = $req->category_id;

    	if($req->file('image') != null){
    		$image = $req->file('image');
    		$image_name = time().".".$image->getClientOriginalExtension();

    		$destination = "images/";

    		$image->move($destination, $image_name);

    		$editOperator->imgPath = $destination.$image_name;
    	}

    	$editOperator->save();

    	Session::flash("message", "$editOperator->name has been updated!");

    	//return to catalog
    	return redirect('/operators');
    }

    public function addToCart($id, Request $req){
    	
    	//case if session is existing
    	if(Session::has('cart')){
    		$cart = Session::get('cart');
    	}

    	//case ifcart is initially empty
    	else{
    		$cart = [];
    	}

    	if(isset($cart[$id])){
    		$cart[$id] += $req->quantity;
    	}

    	else{
    		$cart[$id] = $req->quantity;
    	}

    	Session::put("cart", $cart);

    	$operator = Item::find($id);

    	Session::flash("message", "$req->quantity instances of $operator->name has been added to your cart!");

    	return redirect()->back();
    }

    public function showCart(){

    	$items = [];
    	$total = 0;

    	if(Session::has('cart')){
    		$cart = Session::get('cart');

    		foreach($cart as $item_id => $quantity){
    			$item = Item::find($item_id);
    			$item->quantity = $quantity;
    			$item->subtotal = $item->price * $quantity;
    			$items[] = $item;
    			$total += $item->subtotal;
    		}

    		return view('userviews.cart', compact('items', 'total'));
   		}
   	}

}