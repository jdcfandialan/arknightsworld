@extends('layouts.app')
@section('content')
	<h1 class="text-center py-5">Add Operator</h1>

	<div class="row">
		<div class="col-lg-6 offset-lg-3">
			<form action="/add-operator" method="POST" enctype="multipart/form-data">
				@csrf
				<div class="form-group">
					<label for="name">Operator Name:</label>
					<input type="text" name="name" class="form-control">
				</div>
				<div class="form-group">
					<label for="description">Description:</label>
					<input type="text" name="description" class="form-control">
				</div>
				<div class="form-group">
					<label for="price">Price:</label>
					<input type="number" name="price" class="form-control">
				</div>
				<div class="form-group">
					<label for="image">Image:</label>
					<input type="file" name="image" class="form-control">
				</div>
				<div class="form-group">
					<label for="category_id">Category:</label>
					<select name="category_id" class="form-control">
						<option selected disabled>Select a category</option>
						@foreach($categories as $category)
							<option value="{{ $category->id }}">{{ $category->name }}</option>
						@endforeach
					</select>
				</div>
				<button type="submit" class="btn btn-info">Add item</button>
			</form>
		</div>
	</div>
@endsection