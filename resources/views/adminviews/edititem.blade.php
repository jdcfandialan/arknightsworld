@extends('layouts.app')
@section('content')
	<h1 class="text-center py-5">Update Operator</h1>

	<div class="row">
		<div class="col-lg-6 offset-lg-3">
			<form action="/edit-operator/{{ $item->id }}" method="POST" enctype="multipart/form-data">
				@csrf
				@method('PATCH')
				<div class="form-group">
					<label for="name">Operator Name:</label>
					<input type="text" name="name" class="form-control" value="{{ $item->name }}">
				</div>
				<div class="form-group">
					<label for="description">Description:</label>
					<input type="text" name="description" class="form-control" value="{{ $item->description }}">
				</div>
				<div class="form-group">
					<label for="price">Price:</label>
					<input type="number" name="price" class="form-control" value="{{ $item->price }}">
				</div>
				<div class="form-group">
					<label for="image">Image:</label>
					<input type="file" name="image" class="form-control">
				</div>
				<div class="form-group">
					<label for="category_id">Category:</label>
					<select name="category_id" class="form-control">
						<option disabled>Select a category</option>
						@foreach($categories as $category)
							<option value="{{ $item->category_id }}" {{ $item->category_id == $category->id ? "selected" : ""}}>{{ $category->name }}</option>
						@endforeach
					</select>
				</div>
				<button type="submit" class="btn btn-info">Update Operator</button>
			</form>
		</div>
	</div>
@endsection