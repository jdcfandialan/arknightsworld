@extends('layouts.app')
@section('content')
	
	<h1 class="text-center py-5">Operators</h1>

	@if(Session::has("message"))
		<h4 class="text-center">{{ Session::get("message") }}</h4>
	@endif

	<div class="container">
		<div class="row">
			@foreach($items as $item)
				<div class="col-lg-3 my-2">
					<div class="card">
						<div class="d-flex align-items-center justify-content-center">
							<img src="{{ $item->imgPath }}" class="card-img-top" alt="{{ $item->imgPath }}" style="object-fit: contain;">
						</div>
						<div class="card-body">
							<h4 class="card-title text-center">{{ $item->name }}</h4>
							<hr>
							<p class="card-text font-weight-bold">{{ $item->category->name }}</p>
							<p class="card-text" style="height: 100px">{{ $item->description }}</p>
							<p class="card-text">PHP {{ $item->price }}</p>
						</div>
						<div class="card-footer">
							<form action="/deleteoperator/{{ $item->id }}" method="POST">
							@csrf
							@method('DELETE')
								<button class="btn btn-danger" type="submit">Delete</button>
								<a href="/edit-operator/{{ $item->id }}" class="btn btn-primary"> Edit</a>
							</form>
						</div>
						<div class="card-footer">
							<form action="/addtocart/{{ $item->id }}" method="POST">
								@csrf
								<input type="number" name="quantity" class="form-control" value="1">
								<button type="submit" class="btn btn-success btn-block">Add to Cart</button>
							</form>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>

@endsection